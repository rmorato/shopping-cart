<?php
namespace ShoppingCart\Test\Unit\Domain\Cart\Validations;

use PHPUnit\Framework\TestCase;
use ShoppingCart\Domain\Cart\Cart;
use ShoppingCart\Domain\Product\Product;
use ShoppingCart\Domain\Cart\Validations\ProductNotInCart;
use ShoppingCart\Domain\Cart\Exceptions\ProductNotInCartException;

class ProductNotInCartTest extends TestCase
{
    public function testValidate(): void
    {
        $product = $this->createMock(Product::class);
        $cart = $this->createMock(Cart::class);
        $cart->method('getProduct')->willReturn([
            Cart::QUANTITY => 1,
            Cart::ITEM => $product
        ]);
        $data[ProductNotInCart::CART] = $cart;
        $data[ProductNotInCart::PRODUCT] = $product;
        $validator = new ProductNotInCart();
        
        $this->assertNull($validator->validate($data));
    }

    public function testValidateThrowsException(): void
    {
        $product = $this->createMock(Product::class);
        $cart = $this->createMock(Cart::class);
        $cart->method('getProduct')->willReturn([]);
        $data[ProductNotInCart::CART] = $cart;
        $data[ProductNotInCart::PRODUCT] = $product;
        $validator = new ProductNotInCart();

        $this->expectException(ProductNotInCartException::class);

        $validator->validate($data);
    }
}

<?php
namespace ShoppingCart\Tests\Unit\Application\Cart\DeleteProduct;

use PHPUnit\Framework\TestCase;
use ShoppingCart\Application\Cart\DeleteProduct\DeleteProduct;
use ShoppingCart\Application\Cart\DeleteProduct\DeleteProductRequest;
use ShoppingCart\Application\Cart\DeleteProduct\DeleteProductResponse;
use ShoppingCart\Application\Cart\DeleteProduct\DeleteProductValidator;
use ShoppingCart\Domain\Cart\Cart;
use ShoppingCart\Domain\Cart\CartRepositoryInterface;
use ShoppingCart\Domain\Product\Product;
use ShoppingCart\Domain\Product\ProductRepositoryInterface;

class DeleteProductTest extends TestCase
{
    public function testDeleteProduct(): void
    {
        $cart = $this->createMock(Cart::class);
        $cart->method('deleteProduct');
        $cartRepository = $this->createMock(CartRepositoryInterface::class);
        $cartRepository->method('getById')->willReturn($cart);
        $cartRepository->method('save');
        $product = $this->createMock(Product::class);
        $productRepository = $this->createMock(ProductRepositoryInterface::class);
        $productRepository->method('getById')->willReturn($product);
        $validator = $this->createMock(DeleteProductValidator::class);
        $validator->method('validate')->willReturn(null);
        $request = $this->createMock(DeleteProductRequest::class);
        $request->method('getCartId')->willReturn(1);
        $request->method('getProductId')->willReturn(2);

        $deleteProduct = new DeleteProduct($cartRepository, $productRepository, $validator);

        $this->assertInstanceOf(DeleteProductResponse::class, $deleteProduct($request));
    }
}

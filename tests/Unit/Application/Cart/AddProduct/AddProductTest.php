<?php
namespace ShoppingCart\Tests\Unit\Application\Cart\AddProduct;

use PHPUnit\Framework\TestCase;
use ShoppingCart\Application\Cart\AddProduct\AddProduct;
use ShoppingCart\Application\Cart\AddProduct\AddProductRequest;
use ShoppingCart\Application\Cart\AddProduct\AddProductResponse;
use ShoppingCart\Application\Cart\AddProduct\AddProductValidator;
use ShoppingCart\Domain\Cart\Cart;
use ShoppingCart\Domain\Cart\CartRepositoryInterface;
use ShoppingCart\Domain\Product\Product;
use ShoppingCart\Domain\Product\ProductRepositoryInterface;

class AddProductTest extends TestCase
{
    public function testAddProduct(): void
    {
        $cart = $this->createMock(Cart::class);
        $cart->method('addProduct');
        $cartRepository = $this->createMock(CartRepositoryInterface::class);
        $cartRepository->method('getById')->willReturn($cart);
        $cartRepository->method('save');
        $product = $this->createMock(Product::class);
        $productRepository = $this->createMock(ProductRepositoryInterface::class);
        $productRepository->method('getById')->willReturn($product);
        $validator = $this->createMock(AddProductValidator::class);
        $validator->method('validate')->willReturn(null);
        $request = $this->createMock(AddProductRequest::class);
        $request->method('getCartId')->willReturn(1);
        $request->method('getProductId')->willReturn(2);
        $request->method('getQuantity')->willReturn(3);

        $addProduct = new AddProduct($cartRepository, $productRepository, $validator);

        $this->assertInstanceOf(AddProductResponse::class, $addProduct($request));
    }
}

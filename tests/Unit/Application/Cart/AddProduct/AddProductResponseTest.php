<?php
namespace ShoppingCart\Tests\Unit\Application\Cart\AddProduct;

use PHPUnit\Framework\TestCase;
use ShoppingCart\Application\Cart\AddProduct\AddProductResponse;

class AddProductResponseTest extends TestCase
{
    public function testCreateResponse(): void
    {
        $response = new AddProductResponse();

        $this->assertInstanceOf(AddProductResponse::class, $response);
    }
}

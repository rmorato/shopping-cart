<?php
namespace ShoppingCart\Tests\Unit\Application\Cart\CalculateImport;

use PHPUnit\Framework\TestCase;
use ShoppingCart\Application\Cart\CalculateImport\CalculateImportResponse;

class CalculateImportResponseTest extends TestCase
{
    public function testCreateResponseWithoutTransformedValue(): void
    {
        $import = 10.0;
        $response = new CalculateImportResponse($import);

        $this->assertInstanceOf(CalculateImportResponse::class, $response);
        $this->assertSame($import, $response->getImport());
    }
}

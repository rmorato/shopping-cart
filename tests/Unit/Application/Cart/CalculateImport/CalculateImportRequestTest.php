<?php
namespace ShoppingCart\Tests\Unit\Application\Cart\CalculateImport;

use PHPUnit\Framework\TestCase;
use ShoppingCart\Application\Cart\CalculateImport\CalculateImportRequest;
use ShoppingCart\Domain\Currency\CurrencyList;

class CalculateImportRequestTest extends TestCase
{
    public function testCreateRequest(): void
    {
        $cartId = 1;
        $request = new CalculateImportRequest($cartId);

        $this->assertInstanceOf(CalculateImportRequest::class, $request);
        $this->assertSame($cartId, $request->getCartId());
    }
}

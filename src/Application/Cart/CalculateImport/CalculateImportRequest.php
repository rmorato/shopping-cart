<?php
namespace ShoppingCart\Application\Cart\CalculateImport;

class CalculateImportRequest
{
    /** @var int $cartId */
    private $cartId;

    /**
     * @param integer $cartId
     */
    public function __construct(int $cartId)
    {
        $this->cartId = $cartId;
    }

    public function getCartId(): int
    {
        return $this->cartId;
    }
}

<?php
namespace ShoppingCart\Application\Cart\DeleteProduct;

use ShoppingCart\Domain\Cart\Validations\CartValidatorInterface;
use ShoppingCart\Domain\Cart\Validations\ProductNotInCart;
use ShoppingCart\Domain\Common\ValidatorInterface;
use ShoppingCart\Domain\Common\Validator;

class DeleteProductValidator implements CartValidatorInterface
{
    /**
     * @param array $data
     * @return void
     * @throws ProductNotInCart
     */
    public function validate(array $data)
    {
        $validator = $this->buildValidator();
        $validator->validate($data);
    }

    private function buildValidator(): ValidatorInterface
    {
        return new Validator([
            new ProductNotInCart(),
        ]);
    }
}

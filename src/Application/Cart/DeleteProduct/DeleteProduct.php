<?php
namespace ShoppingCart\Application\Cart\DeleteProduct;

use ShoppingCart\Domain\Cart\Cart;
use ShoppingCart\Domain\Cart\CartRepositoryInterface;
use ShoppingCart\Domain\Cart\Validations\CartValidatorInterface;
use ShoppingCart\Domain\Product\Product;
use ShoppingCart\Domain\Product\ProductRepositoryInterface;

class DeleteProduct
{
    /** @var CartRepositoryInterface $cartRepository */
    private $cartRepository;

    /** @var ProductRepositoryInterface $productRepository */
    private $productRepository;

    /** @var CartValidatorInterface $validator */
    private $validator;

    public function __construct(
        CartRepositoryInterface $cartRepository,
        ProductRepositoryInterface $productRepository,
        CartValidatorInterface $validator
    ) {
        $this->cartRepository = $cartRepository;
        $this->productRepository = $productRepository;
        $this->validator = $validator;
    }
    
    /**
     * @param DeleteProductRequest $request
     * @return DeleteProductResponse
     * @throws CartNotFoundException
     * @throws ProductNotFoundException
     * @throws UnexpectedErrorSavingCartException
     * @throws ProductNotInCart
     */
    public function __invoke(DeleteProductRequest $request): DeleteProductResponse
    {
        $cartId = $request->getCartId();
        $productId = $request->getProductId();

        /** @var Cart $cart */
        $cart = $this->cartRepository->getById($cartId);
        /** @var Product $product */
        $product = $this->productRepository->getById($productId);

        // TODO: Rethink validations (Do these into the domain objects)
        $this->validator->validate([
            CartValidatorInterface::CART => $cart,
            CartValidatorInterface::PRODUCT => $product,
        ]);
        $cart->deleteProduct($product);

        $this->cartRepository->save($cart);

        return new DeleteProductResponse();
    }
}

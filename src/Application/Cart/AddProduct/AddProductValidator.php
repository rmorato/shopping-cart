<?php
namespace ShoppingCart\Application\Cart\AddProduct;

use ShoppingCart\Domain\Cart\Validations\CartValidatorInterface;
use ShoppingCart\Domain\Cart\Validations\MaxOfProductsInCart;
use ShoppingCart\Domain\Cart\Validations\MaxUnitsOfProductInCart;
use ShoppingCart\Domain\Common\ValidatorInterface;
use ShoppingCart\Domain\Common\Validator;

class AddProductValidator implements CartValidatorInterface
{
    /**
     * @param array $data
     * @return void
     * @throws MaxOfProductsInCartException
     * @throws MaxUnitsOfProductInCartException
     */
    public function validate(array $data)
    {
        $validator = $this->buildValidator();
        $validator->validate($data);
    }

    private function buildValidator(): ValidatorInterface
    {
        return new Validator([
            new MaxOfProductsInCart(),
            new MaxUnitsOfProductInCart(),
        ]);
    }
}

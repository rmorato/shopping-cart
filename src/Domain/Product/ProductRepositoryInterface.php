<?php
namespace ShoppingCart\Domain\Product;

use ShoppingCart\Domain\Product\Exceptions\ProductNotFoundException;

interface ProductRepositoryInterface
{
    /**
     * @param integer $id
     * @return Product
     * @throws ProductNotFoundException
     */
    public function getById(int $id): Product;
}

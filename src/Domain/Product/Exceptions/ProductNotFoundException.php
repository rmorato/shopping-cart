<?php
namespace ShoppingCart\Domain\Product\Exceptions;

class ProductNotFoundException extends \Exception
{
    const CODE = 101;
}
<?php
namespace ShoppingCart\Domain\Cart\Exceptions;

class ProductNotInCartException extends \Exception
{
    const CODE = 206;
}
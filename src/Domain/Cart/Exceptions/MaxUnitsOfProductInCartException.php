<?php
namespace ShoppingCart\Domain\Cart\Exceptions;

class MaxUnitsOfProductInCartException extends \Exception
{
    const CODE = 205;
}
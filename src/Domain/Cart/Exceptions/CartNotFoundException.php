<?php
namespace ShoppingCart\Domain\Cart\Exceptions;

class CartNotFoundException extends \Exception
{
    const CODE = 201;
}
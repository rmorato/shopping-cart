<?php
namespace ShoppingCart\Domain\Cart\Exceptions;

class UnexpectedErrorSavingCartException extends \Exception
{
    const CODE = 202;
}
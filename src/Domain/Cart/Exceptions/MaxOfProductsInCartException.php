<?php
namespace ShoppingCart\Domain\Cart\Exceptions;

class MaxOfProductsInCartException extends \Exception
{
    const CODE = 204;
}
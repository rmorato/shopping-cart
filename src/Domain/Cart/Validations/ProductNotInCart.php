<?php
namespace ShoppingCart\Domain\Cart\Validations;

use ShoppingCart\Domain\Cart\Cart;
use ShoppingCart\Domain\Product\Product;
use ShoppingCart\Domain\Cart\Exceptions\ProductNotInCartException;

class ProductNotInCart implements CartValidatorInterface
{
    public function validate(array $data)
    {
        /** @var Cart $cart */
        $cart = $data[self::CART];
        /** @var Product $product */
        $product = $data[self::PRODUCT];

        $productInCart = $cart->getProduct($product);
        if (empty($productInCart)) {
            throw new ProductNotInCartException();
        }
    }
}

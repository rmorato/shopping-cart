<?php
namespace ShoppingCart\Domain\Cart\Validations;

use ShoppingCart\Domain\Cart\Cart;
use ShoppingCart\Domain\Cart\Exceptions\MaxOfProductsInCartException;

class MaxOfProductsInCart implements CartValidatorInterface
{
    const MAX_PRODUCTS = 10;

    public function validate(array $data)
    {
        /** @var Cart $cart */
        $cart = $data[self::CART];

        $productsInCart = $cart->getAllProducts();
        if (count($productsInCart) >= self::MAX_PRODUCTS) {
            throw new MaxOfProductsInCartException();
        }
    }
}

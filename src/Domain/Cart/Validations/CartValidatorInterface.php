<?php
namespace ShoppingCart\Domain\Cart\Validations;

use ShoppingCart\Domain\Common\ValidatorInterface;

interface CartValidatorInterface extends ValidatorInterface
{
    const CART= 'cart';
    const PRODUCT= 'product';
}

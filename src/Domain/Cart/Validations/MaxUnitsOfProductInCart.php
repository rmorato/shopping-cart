<?php
namespace ShoppingCart\Domain\Cart\Validations;

use ShoppingCart\Domain\Cart\Cart;
use ShoppingCart\Domain\Product\Product;
use ShoppingCart\Domain\Cart\Exceptions\MaxUnitsOfProductInCartException;

class MaxUnitsOfProductInCart implements CartValidatorInterface
{
    const MAX_UNITS_OF_PRODUCT = 50;

    public function validate(array $data)
    {
        /** @var Cart $cart */
        $cart = $data[self::CART];
        /** @var Product $product */
        $product = $data[self::PRODUCT];

        $productInCart = $cart->getProduct($product);
        if ($productInCart[Cart::QUANTITY] >= self::MAX_UNITS_OF_PRODUCT) {
            throw new MaxUnitsOfProductInCartException();
        }
    }
}

<?php
namespace ShoppingCart\Domain\Common;

interface ValidatorInterface
{
    const CART_REPOSITORY = 'cart-repository';
    const PRODUCT_REPOSITORY = 'product-repository';

    public function validate(array $data);
}
